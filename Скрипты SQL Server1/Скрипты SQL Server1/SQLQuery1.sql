IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = 'MyDB')
	CREATE DATABASE MyDB --�������� ���� ������, �� ������ � ������, ���� �� ���� ���
/* ������ �������� ������� � ����������, �������������� ��������, ��� �� ��� ���
�������� � ��� ��������, ���� ������� � ����� ������ ����, � ��� ������ �� ������ ������. ���������� � ��� ���� ������ ������ ����*/

IF NOT EXISTS (SELECT * FROM sysobjects  WHERE name = 'Personal' and xtype='U')
BEGIN
    CREATE TABLE MyDB.dbo.Personal (
        ID int IDENTITY (1,1),
		Name nvarchar(MAX),
		Age int,
		IsMan bit,
		PersonalAddress nvarchar(MAX), --�� ���� �������� �� ������ �����, �� ��� �������� �����
		Phone nvarchar(MAX), --���������� ����� �� ��� �������� ��� �������, � ����� ������� ��� �������� �������, ������� ����� ����� ����� �����. �� ���� ������ ������ ����� ����, ���� �������� ��� ����� ���������, �� ��������
		Passport nvarchar(MAX),
		PostID int, --������� ������������ ��������� �������� �� ��������������
		primary key (ID)
	);
END
go
INSERT INTO MyDB.dbo.Personal (
	Name,
	Age,
	IsMan,
	PersonalAddress,
	Phone,
	Passport,
	PostID)
VALUES (
	'Ivanov6',
	23,
	1,
	'SomeAddress',
	123456789,
	'SomePassport',
	1)